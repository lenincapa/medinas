module ApplicationHelper
	def local_date(date, tipo)
		if date.nil? == true
			date = "sin fecha"
		else
			case tipo
			when tipo = "date"
				date = date.strftime("%Y-%m-%d")
			when tipo = "datetime"
				date = date.strftime("%Y-%m-%d | %H:%M:%S")
			when tipo = "time"
				date = date.strftime("%H:%M")
			end
		end
		date
	end
end
