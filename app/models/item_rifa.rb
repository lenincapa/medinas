# == Schema Information
#
# Table name: item_rifas
#
#  id         :integer          not null, primary key
#  fecha      :datetime
#  cantidad   :float
#  rifa_id    :integer
#  socio_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class ItemRifa < ActiveRecord::Base
	belongs_to :rifa
	belongs_to :socio
end
