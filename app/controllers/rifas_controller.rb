class RifasController < ApplicationController
	
	def index
		@socios = Socio.all
		@rifa = Rifa.last
		unless @rifa.nil?
			@grouped = @rifa.item_rifas.group_by &:socio_id
		end
	end

	def new
		@rifa = Rifa.new
	end

	def create
		@rifa = Rifa.new(rifa_params)
		@rifa.save
		respond_to do |format|
			format.js
		end
	end

	private

	def rifa_params
		params.require(:rifa).permit :periodo
	end
end
