class SociosController < ApplicationController
	# before_filter :require_login
  before_action :find_socio, only: [:show, :edit, :update, :destroy]
	def new
		@socio = Socio.new
	end

	def index
    @socios = Socio.all
  end

	def create
    @socio = Socio.new(socio_params)
    if @socio.save
    	redirect_to root_path, :notice => "Almacenado"
    else
    	redirect_to root_path, :notice => "Error"
    end
	end
  
  def show
    respond_to do |format|
      format.html
      format.js{ render "show" }
    end
  end


  def edit
    respond_to do |format|
      format.html
      format.js{ render "new_or_edit" }
    end
  end

  def update
    respond_to do |format|
      @socio.update(socio_params)
      format.html
      format.js { render "success"}
    end
  end

	private
	def find_socio
		@socio = Socio.find(params[:id])
	end

	def socio_params
		params.require(:socio).permit(:nombre, :direccion, :cedula, :telefono)
	end
end
