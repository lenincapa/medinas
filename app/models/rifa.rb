# == Schema Information
#
# Table name: rifas
#
#  id         :integer          not null, primary key
#  periodo    :datetime
#  created_at :datetime
#  updated_at :datetime
#

class Rifa < ActiveRecord::Base
	has_many :item_rifas

	after_save :create_items

	def create_items
		socios = Socio.all
		itemrifas = []
		itemrifa = nil
		contador = 0
		socios.each do |socio|
			for i in(0..11)
				fecha = self.periodo.to_time.beginning_of_year + i.month
				itemrifa = ItemRifa.new(:rifa_id => self.id, :socio_id => socio.id, :fecha => fecha)
				itemrifa.save
			end
		end
		# item_rifas << itemrifa
		# self.item_rifas << itemrifas
		# self.save
	end
end
