class ItemRifasController < ApplicationController
	
	before_action :find_item, only: [:pay]

	def new
		@item_rifa = ItemRifa.new
	end

	def create
		@item_rifa = ItemRifa.new(item_rifa_params)
		@item_rifa.save
		respond_to do |format|
			format.js
		end
	end

	def pay
		@item.cantidad = 1
		@item.save
		respond_to do |format|
			format.html
			format.js
		end
	end

	private

	def item_rifa_params
		params.require(:item_rifa).permit :socio_id,
		:rifa_id,
		:fecha,
		:cantidad
	end

	def find_item
		@item = ItemRifa.find(params[:item_rifa_id])
	end
end
