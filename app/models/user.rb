# == Schema Information
#
# Table name: users
#
#  id               :integer          not null, primary key
#  username         :string(255)      not null
#  crypted_password :string(255)      not null
#  salt             :string(255)      not null
#  suspendido       :boolean          default(FALSE)
#  socio_id         :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class User < ActiveRecord::Base
  authenticates_with_sorcery!
end
