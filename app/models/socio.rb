# == Schema Information
#
# Table name: socios
#
#  id         :integer          not null, primary key
#  nombre     :string(255)
#  apellido   :string(255)
#  cedula     :string(255)
#  telefono   :string(255)
#  direccion  :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Socio < ActiveRecord::Base
	has_many :item_rifas
end
