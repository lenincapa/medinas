class CreateSocios < ActiveRecord::Migration
  def change
    create_table :socios do |t|
    	t.string :nombre
    	t.string :apellido
    	t.string :cedula
    	t.string :telefono
    	t.string :direccion
      t.timestamps
    end
  end
end
