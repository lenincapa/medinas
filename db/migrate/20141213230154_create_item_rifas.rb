class CreateItemRifas < ActiveRecord::Migration
  def change
    create_table :item_rifas do |t|
    	t.datetime :fecha
    	t.float :cantidad, :default => 0
    	t.references :rifa, index: true
    	t.references :socio, index: true
      t.timestamps
    end
  end
end
